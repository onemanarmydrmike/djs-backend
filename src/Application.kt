package com.pl.mm.university.lab

import com.pl.mm.university.lab.model.User
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.auth.UserIdPrincipal
import io.ktor.auth.authenticate
import io.ktor.auth.basic
import io.ktor.features.CORS
import io.ktor.features.ContentNegotiation
import io.ktor.gson.gson
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import java.text.DateFormat

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(CORS) {
        method(HttpMethod.Options)
        header(HttpHeaders.AccessControlAllowOrigin)
        anyHost()
        host("localhost:3000")
        allowCredentials = true
        allowNonSimpleContentTypes = true
    }
    install(Authentication) {
        basic(name = "basicAuth") {
            validate { credentials ->
                if (credentials.name == "michal") {
                    UserIdPrincipal(credentials.name)
                } else {
                    null
                }
            }
        }
    }
    install(ContentNegotiation) {
        gson {
            setDateFormat(DateFormat.LONG)
            setPrettyPrinting()
        }
    }

    routing {
        get("/") {
            call.respondText("HELLO WORLD!", contentType = ContentType.Text.Plain)
        }

        authenticate("basicAuth") {
            /*
            TODO
            Let's try to use basicAuth and move the endpoints here
             */
        }

        post("/login") {
            val user = call.receive<User>()
            val existingUser = MongoDAO.getUser(user.username)
            if (existingUser != null && existingUser.password == user.password) {
                call.respond(User(username = existingUser.username, sentence = existingUser.sentence))
            } else {
                call.respondText("Invalid username or password", contentType = ContentType.Text.Plain)
            }
        }

        post("/logout") {
            call.respondText(
                "You are successfully logged out", contentType = ContentType.Text.Plain
            )
        }

        post("/register") {
            try {
                val user = call.receive<User>()
                val existingUser = MongoDAO.getUser(user.username)
                if (existingUser == null) {
                    MongoDAO.createUser(user)
                    call.respond(user)
                } else {
                    call.respondText("{\"error\": \"user ${user.username} already exists\"}", contentType = ContentType.Application.Json)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        post("/updateSentence") {
            val user = call.receive<User>()
            MongoDAO.updateUser(user)
            call.respond(user)
        }
    }
}