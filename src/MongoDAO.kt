package com.pl.mm.university.lab

import com.mongodb.MongoClient
import com.pl.mm.university.lab.model.DBUser
import com.pl.mm.university.lab.model.User
import org.litote.kmongo.*
import java.lang.Exception

object MongoDAO {

    private var mongoClient: MongoClient = KMongo.createClient()
    private val db = mongoClient.getDatabase("djs")
    private val users = db.getCollection<DBUser>("users")

    fun createUser(user: User) {
        val dbUser = DBUser(_id = user.username, password = user.password!!)
        users.insertOne(dbUser)
    }

    fun updateUser(user: User) {
        val existingUser = getUser(user.username)
        if (existingUser != null) {
            val updatedUser = DBUser(_id = user.username, password = existingUser.password!!, sentence = user.sentence!!)
            try {
                users.updateOne(updatedUser)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getUser(username: String): User? {
        val dbUser = users.findOne(DBUser::_id eq username)
        return if (dbUser != null) {
            User(username = dbUser._id, password = dbUser.password, sentence = dbUser.sentence)
        } else {
            null
        }
    }
}